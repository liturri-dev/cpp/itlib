#include "dates.hxx"
#include <locale>
// #include <ctime>

int main()
{
  std::locale loc("");
  std::cout << loc.name() << std::endl;
  // setlocale(LC_ALL, "");
  itlib::date Fecha;
  itlib::date Fecha2("1999-02-07 10:32:12-0200");

  Fecha2.SetDay(01);
  Fecha2.SetYear(99);

  std::cout << Fecha.GetSQLDateTime() << '\n'
            << Fecha2.GetSQLDateTime() << std::endl;

  if (Fecha == Fecha2)
    std::cout << "Fecha == Fecha2" << std::endl;
  else if (Fecha > Fecha2)
    std::cout << "Fecha > Fecha2" << std::endl;
  else if (Fecha < Fecha2)
    std::cout << "Fecha < Fecha2" << std::endl;

  Fecha = Fecha2;
  std::cout << Fecha.GetSQLDateTime() << '\n'
            << Fecha2.GetSQLDateTime() << std::endl;
  return 0;
}